package electric

import (
	"fmt"

	"gitlab.com/phinicota/go.networks/pkg/graph"
)

// Node represents an electric connection
type Node struct {
	in, out []*Z
	*graph.Node
}

// NewNode constructs an empty Node
func NewNode(name string) *Node {
	return &Node{
		in:   []*Z{},
		out:  []*Z{},
		Node: graph.NewNode(name),
	}
}

// In returns all entering edges
// Satisfies graph.Node
func (n *Node) In() []*Z {
	return n.in
}

// AddIn adds a new entering edge
func (n *Node) AddInImpedance(z *Z) {
	n.in = append(n.in, z)
	n.AddIn(z.Edge)
}

// Out returns all outgoing edges
// Satisfies graph.Node
func (n *Node) Out() []*Z {
	return n.out
}

// AddOut adds a new outgoing edge
// Satisfies graph.Node
func (n *Node) AddOutImpedance(z *Z) {
	n.in = append(n.in, z)
	n.AddOut(z.Edge)
}

// Z represents a complex impedance
type Z struct {
	*graph.Edge
	complex128
}

// NewZ constructs a new unconnected Z
func NewZ(c complex128) *Z {
	return &Z{
		complex128: c,
		Edge:       &graph.Edge{},
	}
}

// String satisfies Stringer
func (z *Z) String() string {
	return fmt.Sprintf("%vohm + jw%vohm, %v",
		z.R(), z.X(), z.Edge)
}

// To connects to a Node in
// Satisfies graph.Edge
func (z *Z) To(n *Node) *Z {
	z.Edge.To(n.Node)
	n.AddInImpedance(z)
	return z
}

// From connects to a Node out
// Satisfies graph.Edge
func (z *Z) From(n *Node) *Z {
	z.Edge.From(n.Node)
	n.AddOutImpedance(z)
	return z
}

// R returns resistive component of the impedance
// Satisfies networks.Resistance
func (z Z) R() float64 {
	return real(z.complex128)
}

// X returns reactive component of the impedance
// Satisfies networks.Reactance
func (z Z) X() float64 {
	return imag(z.complex128)
}

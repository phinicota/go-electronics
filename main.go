package main

import (
	"fmt"

	"gitlab.com/phinicota/go.networks/pkg/electric"
)

func main() {
	z1 := electric.NewZ(complex(10, 10))
	z2 := electric.NewZ(complex(10, 10))
	z3 := electric.NewZ(complex(10, 10))

	n1 := electric.NewNode("n1")
	n2 := electric.NewNode("n2")

	z1.From(n1).To(n2)
	z2.From(n1).To(n2)
	z3.From(n1).To(n2)

	fmt.Println("n1 in:", n1.In())
	fmt.Println("n1 out:", n1.In())

	circuit := electric.NewCircuit()
	circuit.AddNode(n1.Node)
	circuit.AddNode(n2.Node)

	fmt.Println(circuit)
}
